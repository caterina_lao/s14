// sample comments - can make comments in a single line - ctrl + /

/* multi line
comments in JS is much like CSS and HTML, cannot be read by the browser.
This comments are often used to add notes and to add markers to your code crtl + shift + //
*/

console.log ('I am external');


let num = 10;
console.log(num);

myVariable = 'New Initialized Value';
console.log (myVariable);

let myVariable2;
myVariable2 = "Initialized Value";
console.log(myVariable2);

num2 = 5;
console.log(num2);


num2 = 5 + 3;
console.log(num2);

const num1 = 6;
console.log(num1);

// num1 = 0;
// console.log(num1);

// Declaring multiple variables

let brand = 'Toyota', model = 'Vios', type = 'Sedan';
console.log(brand);
console.log(model);
console.log(type);

// console logging of multiple variables: use commas to separate each variables

console.log(brand,model,type);

let country = 'Philippines';
let province = "Manila";

console.log(typeof(country));
console.log(province);

// mini-activity

let firstName ='Catherine', lastName = 'Itil';
console.log(firstName,lastName);

//*mini activity2*/

let word1 = 'I';
let word2 = 'am';
let word3 = 'a';
let word4 = 'bootcamper';
let word5 = 'of';
let word6 = 'Zuitt';
let space = '';
console.log(word1, space, word2, space, word3, space, word4, space, word5, space, word6);

let sentenceLiteral = `${word1} ${word2} ${word3} ${word4} ${word5} ${word6}`;
console.log(sentenceLiteral);

// data type

let numString1 = '7';
let numString2 = '6';
let numA = 7;
let numB = 5;
let num3 = 5.5;
let num4 = .5;

console.log(numString1 + numString2);
console.log(numA + numB);
console.log(numA + num3);
console.log(num3 + num4);

// forced coercion - when one data type is forced to change to complete an operation

// string + num - concatenation

console.log(numString1 + numA);

// parseInt()

console.log(num4 + parseInt(numString1));

// Mathematical Operation

console.log(numA - num3);
console.log(numString2 - numB);
console.log(numString1 - numString2);

// Multiplication

let sample = 'cath';
console.log(num2 * num3);
console.log(numString2 * num4);
console.log(numString1 * numString2);
console.log(sample * num2); /*Output = NaN*/
console.log(sample * numString2); /*Output = NaN*/


// Division

console.log(num3 / 0);

// % Modulo - remainder of a division operation

let isAdmin = true;
let isMarried = false;
let isGwapo = true;

console.log('Does Rommel is married?' + isMarried);
console.log(`Is he handsome?` +isGwapo);

// array literal

let array2 = ['Goku', 'Picolo', 'Gohan', 'Vegeta'];
console.log(array2);
let array1 = ['One Punch Man', true, 500, 'Saitama']; /*console.log(array1);  not a good practice*/

let hero = {
	heroName: 'One Punch Man',
	isActive: true,
	salary: 500,
	realName: 'Saitama1',
	height: 200
}
console.log(hero);


let array = ['Boys like Girls', 'A1', 'Vertical Horizon', 'Backstreet Boys'];
console.log(array);


let  me = {
	firstName: 'Caterina',
	lastName: 'Lao',
	isDeveloper: true,
	hasPortfolio: true,
	Age: 15
}
console.log(me);
	

/*null vs undefined*/

let foundResult = null;
let person2 = {
	name: 'Juan',
	age: 35
}
console.log(person2.isAdmin); 

// function

function greet() {
	console.log('Hello, my name is Sitama.');
}

// invoked a function (calling a function) - functionname

greet();
greet();
greet();
greet();
greet();
greet();

// parameters 

function printName (name) {
	console.log(`My name is ${name}`)
}

printName('caterina');
// When fcn is invoked and data passed, we call the data as argument.
// In this invocation, 'caterina' is an argument passed into our printName fcn and is represeted by the "name". "name" - the parameter within our Function.

function displayNum(number) {
	console.log(number);
}

displayNum(5000);

// Multiple Parameters

function displayFullName(firstName, lastName, age) {
	console.log(`${firstName}, ${lastName} ${age}`)
}

displayFullName('Juan', 'Masipag', 29);

// Return keyword

function createName (firstName, lastName) {
	return `${firstName} ${lastName}`
	console.log("I will no longer run before the function calue reult has been return");
}

createName('Tom',  'Cruise');